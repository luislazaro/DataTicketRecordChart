﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models.Rules;
using WebApplication1.Models.Domain;
using System.Web.Mvc;
using DataTicketChart.Models.Domain;
using DataTicketChart.Models.Entities;

namespace DataTicketChart.Controllers
{
    public class DataTicketController : Controller
    {
        Mail objCorreo = new Mail();
        TicketSocioFile objTicket = new TicketSocioFile();
        MuscularCalc objMuscularCalc = new MuscularCalc();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClientRecord()
        {
            return View("ClientRecord");
        }

        /*[ActionName("GetClients")]
        public ActionResult GetPaisesPorContinente()
        {
            ClientRules clientRules = new ClientRules();
            List<TblSocio> listClients = clientRules.getClients();
            return Json(listClients);
        }*/

        public JsonResult GetClients()
        {
            ClientRules clientRules = new ClientRules();
            List<sel_socio_new_Result> listClients = clientRules.getClients();
            return Json(listClients);
        }
        
        public JsonResult SaveRecord(int client, decimal peso, decimal pesoi, decimal pesob, decimal abdai, decimal abdab, decimal abda, decimal abdbi, decimal abdb, decimal abdbb, decimal caderai, decimal cadera, decimal caderatb, decimal altura, string talla, decimal grasac, decimal imc, decimal ta, decimal guia, string observaciones)
        {
            TblRecord record = new TblRecord
            {
                id_record=client,
                peso = peso,
                pesoi = pesoi,
                pesob = pesob,

                abdai = abdai,
                abdab = abdab,
                abda = abda,

                abdbi = abdbi,
                abdb = abdb,
                abdbb = abdbb,

                caderai = caderai,
                cadera = cadera,
                caderatb = caderatb,

                altura = altura,
                talla = talla,
                grasac = grasac,
                imc = imc,
                ta = ta,
                guia = guia,
                observaciones = observaciones
            };
            RecordRules recordRules = new RecordRules();
            int result = recordRules.SaveRecord(record);
            return Json(result);
        }

        public JsonResult GetChartData(int client)
        {
            ChartDataRules chartDataRules = new ChartDataRules();
            List<TblHistorialPeso> chartData = chartDataRules.GetChartData(client);
            return Json(chartData);
        }

        public JsonResult saveTicket(int client, bool completo)
        {
            ClientRules clientRules = new ClientRules();
            List<TblSocio> socioData = clientRules.getSocioByNumber(client);
            //objTicket.imprimirTicket(Session["idPago"].ToString(), ddlNumero.SelectedItem.Text, txtNombre.Text + " " + txtApPaterno.Text, Session["Nombre"].ToString(), ddlPaquete.SelectedItem.Text, txtFechaPago.Text, txtFecha.Text, txtTotalPagar.Text, txtTotalRecibido.Text, txtAdeudos.Text);
            foreach (TblSocio socio in socioData)
            {
                   objTicket.imprimirTicket(
                   objMuscularCalc.NumSocio_,
                   objMuscularCalc.NombreCompleto_,
                   objMuscularCalc.PliegueTricipal_,
                   objMuscularCalc.PliegueEscapular_,
                   objMuscularCalc.Trigliceridos_,
                   objMuscularCalc.Colesterol_,
                   objMuscularCalc.Glucosa_,
                   objMuscularCalc.FrecCardiRepo_,
                   objMuscularCalc.PresArtSisfo_,
                   objMuscularCalc.PresArtSisDias_,
                   objMuscularCalc.PorCargaPecho_,
                   objMuscularCalc.PorCargaPierna_,
                   objMuscularCalc.MetaboBasal_
                   );
            }
            return Json(socioData);
        }

        public JsonResult sendMail(int client)
        {
            ClientRules clientRules = new ClientRules();
            List<TblSocio> socioData = clientRules.getSocioByNumber(client);
            getMuscularCal(client.ToString());
            foreach (TblSocio socio in socioData)
            {
                String temp = objCorreo.descripciónPago(
                   objMuscularCalc.NumSocio_,
                   objMuscularCalc.NombreCompleto_,
                   objMuscularCalc.PliegueTricipal_,
                   objMuscularCalc.PliegueEscapular_,
                   objMuscularCalc.Trigliceridos_,
                   objMuscularCalc.Colesterol_,
                   objMuscularCalc.Glucosa_,
                   objMuscularCalc.FrecCardiRepo_,
                   objMuscularCalc.PresArtSisfo_,
                   objMuscularCalc.PresArtSisDias_,
                   objMuscularCalc.PorCargaPecho_,
                   objMuscularCalc.PorCargaPierna_,
                   objMuscularCalc.MetaboBasal_
                   );
                objCorreo.mail = socio.mail.ToString();
                objCorreo.Envia(false);
            }
            return Json(socioData);
        }

        public MuscularCalc getMuscularCal(String numSocio) {
            objMuscularCalc.NumSocio_ = numSocio;
            objMuscularCalc.NombreCompleto_ = "";
            objMuscularCalc.PliegueTricipal_ = "";
            objMuscularCalc.PliegueEscapular_ = ""; 
            objMuscularCalc.Trigliceridos_ = "";
            objMuscularCalc.Colesterol_ = "";
            objMuscularCalc.Glucosa_ = "";
            objMuscularCalc.FrecCardiRepo_ = "";
            objMuscularCalc.PresArtSisfo_ = "";
            objMuscularCalc.PresArtSisDias_ = "";
            objMuscularCalc.PorCargaPecho_ = "";
            objMuscularCalc.PorCargaPierna_ = "";
            objMuscularCalc.MetaboBasal_ = "";
            return objMuscularCalc;
       }
    }
}