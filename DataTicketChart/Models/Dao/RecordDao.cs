﻿using agenda_ssw_dtic.Models.Utils;
using DataTicketChart.Models.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Models.Domain;                                

namespace WebApplication1.Models.Dao
{
    public class RecordDao
    {
        public int SaveRecord(TblRecord client) {
            int result=-1;
            string nameStoredProcedure = "add_HistorialPesoSocio";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStringSSW"].ConnectionString);
            SqlCommand cmd = new SqlCommand(nameStoredProcedure, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id_socio", client.id_record));
            cmd.Parameters.Add(new SqlParameter("@peso", client.peso));
            cmd.Parameters.Add(new SqlParameter("@pesoi", client.pesoi ));
            cmd.Parameters.Add(new SqlParameter("@pesob", client.pesob ));
            cmd.Parameters.Add(new SqlParameter("@abdai", client.abdai));
            cmd.Parameters.Add(new SqlParameter("@abda", client.abda));
            cmd.Parameters.Add(new SqlParameter("@abdab", client.abdab));
            cmd.Parameters.Add(new SqlParameter("@abdbi", client.abdbi));
            cmd.Parameters.Add(new SqlParameter("@abdb", client.abdb));
            cmd.Parameters.Add(new SqlParameter("@abdbb", client.abdbb));
            cmd.Parameters.Add(new SqlParameter("@caderai", client.caderai));
            cmd.Parameters.Add(new SqlParameter("@cadera", client.cadera));
            cmd.Parameters.Add(new SqlParameter("@caderatb", client.caderatb));
            cmd.Parameters.Add(new SqlParameter("@altura", client.altura));
            cmd.Parameters.Add(new SqlParameter("@talla", client.talla));
 
            cmd.Parameters.Add(new SqlParameter("@grasac", client.grasac) );
            cmd.Parameters.Add(new SqlParameter("@imc", client.imc));
            cmd.Parameters.Add(new SqlParameter("@ta", client.ta));
            cmd.Parameters.Add(new SqlParameter("@guia", client.guia));
            cmd.Parameters.Add(new SqlParameter("@observaciones", client.observaciones));

            conn.Open();
            result = cmd.ExecuteNonQuery();
            conn.Close();
            return result;
        }
    }
}