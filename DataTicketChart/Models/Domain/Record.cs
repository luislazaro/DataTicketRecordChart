﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Domain
{
    public class Record
    {
        public int IdClient { get; set; }

        public double Peso { get; set; }
        public double Pesoi { get; set; }
        public double Pesob { get; set; }

        public double Abdai { get; set; }
        public double Abda { get; set; }
        public double Abdab { get; set; }

        public double Abdbi { get; set; }
        public double Abdb { get; set; }
        public double Abdbb { get; set; }

        public double Caderai { get; set; }
        public double Cadera { get; set; }
        public double Caderatb { get; set; }

        public double Altura { get; set; }
        public string Talla { get; set; }
        public double Grasac { get; set; }
        public double Imc { get; set; }
        public double Ta { get; set; }
        public double Guia { get; set; }
        public string Observaciones { get; set; }
    }
}