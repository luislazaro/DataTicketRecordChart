﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Net.Mime;

namespace WebApplication1.Models.Rules
{
    public class Mail
    {
        MailMessage mmsg = new MailMessage();
        Attachment adjuntos;
        SmtpClient cliente = new SmtpClient();
        String archivos = "";
        List<String> destinatario = new List<String>();
        String destinatarioCopia = "";
        String cuerpo = "";
        String footer = "Este mensaje se dirige exclusivamente a su destinatario. Contiene información CONFIDENCIAL sometida a secreto profesional o cuya divulgación está prohibida por la ley. Si ha recibido este mensaje por error, debe saber que su lectura, copia y uso están prohibidos. Le rogamos que nos lo comunique inmediatamente por esta misma vía o por teléfono y proceda a su destrucción.";


        //public string _mail;

        public string mail { get; set; }
        //{
        //    get
        //    {
        //        return destinatario;
        //    }
        //    set
        //    {
        //        _mail = value;
        //        if (_mail == null)
        //        {
        //            destinatario.Add(_mail);
        //        }
        //    }
        //}

        /// <summary>
        /// Constructor, Asigna el cuerpo
        /// </summary>
        public Mail()
        {
            cuerpo = "<u><b>Propósito:Comprobante de pago</u></b>" +
                         "<br><br>" +
                         "Estimado:Socio <br><br><br>" +
                         "Adjunto podra encontrar la información de su Progreso:" +
                         "<br><br>" +
                         " <br>" +
                         " Favor de revisar tu información, no olvides ponerte en contacto con nosotros para dudas y aclaraciones." +
                         "<br><br>" +
                         "Gracias por ser parte de nuestra familia" +
                         "<br><br>" +
                         "<h6>" + footer + "</h6></br>" +
                         "<h6>/////************************* Por Favor, No respondas este correo  =)   " + DateTime.Now.ToShortDateString() + " SISA - Wellness Lab En Forma 2018. *************************////<br>";
        }


        /// <summary>
        /// Agrega los destinatarios al Objeto del Correo
        /// </summary>
        public void destinatarios()
        {
            //foreach (var item in destinatario)
            //{
            mmsg.To.Add(mail);
            //}
        }


        /// <summary>
        ///  Envia Correos 
        /// </summary>
        public Boolean Envia(bool opcionCabecero)
        {
            try
            {// Ahora creamos la vista para clientes que 
             // pueden mostrar contenido HTML...

                string html = "<img src='cid:imagen' />";

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html,
                                            Encoding.UTF8,
                                            MediaTypeNames.Text.Html);

                // Creamos el recurso a incrustar. Observad
                // que el ID que le asignamos (arbitrario) está
                // referenciado desde el código HTML como origen
                // de la imagen (resaltado en amarillo)...
                AlternateView plainView = AlternateView.CreateAlternateViewFromString(cuerpo, null, MediaTypeNames.Text.Html);
                LinkedResource img = new LinkedResource(@"C:\globos.jpg", MediaTypeNames.Image.Jpeg);
                img.ContentId = "imagen";

                // Lo incrustamos en la vista HTML...
                htmlView.LinkedResources.Add(img);
                //mmsg = new MailMessage(Usuario, PassWord);
                destinatarios();
                //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario
                //Asunto
                mmsg.Subject = opcionCabecero ? "¡Feliz Cumpleaños! " : "Comprobante de Pago";
                mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //Cuerpo del Mensaje
                mmsg.Body = cuerpo;
                //if (opcionCabecero)
                //{
                //    mmsg.AlternateViews.Add(htmlView);
                //    mmsg.AlternateViews.Add(plainView);
                //}
                mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML

                //Correo electronico desde la que enviamos el mensaje
                mmsg.From = new MailAddress("wellnesslabenformamx@gmail.com");
                return clienteCorreo();
            }
            catch (Exception)
            {

                return false;
            }
        }

        public string descripciónPago(String NumSocio, String NombreCompleto, String PliegueTricipal, String PliegueEscapular, String Trigliceridos, String Colesterol, 
            String Glucosa, String FrecCardiRepo, String PresArtSisfo, String PresArtSisDias, String PorCargaPecho, String PorCargaPierna,String MetaboBasal)
        //(imprimirTicket(Session["idPago"].ToString(), ddlNumero.SelectedItem.Value, txtNombre.Text + " " + txtApPaterno.Text, Session["Nombre"].ToString(), ddlPaquete.SelectedItem.Text, txtFechaPago.Text, txtFecha.Text, txtTotalPagar.Text, txtTotalRecibido.Text, txtAdeudos.Text)
        {
            string lines = "<br />" + "<br />" + "<br />" + "WELLNESS LAB EN FORMA" + "<br />" +
                            "EXPEDIDO EN:" + "<br />" +
                            "CALLE ODONTOLOGÍA NO. 13 LOC. 1 COLONIA SPUAZ" + "<br />" +
                            "MEXICO, GPE. ZACATECAS" + "<br />" +
                                " " + "<br />" +
                                "Socio: " + NumSocio + " "+ NombreCompleto + " < br />" +
                                "Fecha de revisión: " + DateTime.Now.ToShortDateString() + "<br />" +
                                " " + "<br />" + "Pliegue Tricipal: " + PliegueTricipal +
                                " " + "<br />" + "Pliegue Escapular: " + PliegueEscapular +
                                " " + "<br />" + "Trigliceridos: " + Trigliceridos +
                                " " + "<br />" + "Colesterol: " + Colesterol +
                                " " + "<br />" + "Glucosa: " + Glucosa +
                                " " + "<br />" + "Frecuencía Cardiaca: " + FrecCardiRepo +
                                " " + "<br />" + "Frecuencía Arterial Sisfolica: " + PresArtSisfo +
                                " " + "<br />" + "Frecuencía Arterial Diasfolica: " + PresArtSisDias +
                                " " + "<br />" + "Frecuencía Arterial Diasfolica: " + PresArtSisDias +
                                "   " + "<br />" +
                                " " + "<br />" + "% Porcentaje Carga Pecho: " + PorCargaPecho +
                                " " + "<br />" + "% Porcentaje Carga Pierna: " + PorCargaPierna +
                                " " + "<br />" + "Metabolismo Basal: " + MetaboBasal +
                                " " + "<br />" + 
                                " " + " " + "<br />" +
                                " " + "<br />" + "TU SALUD ES NUESTRA PASION..." + "<br />" +
                                "VIVE LA EXPERIENCIA WELLNESS LAB EN FORMA" + "<br />" +
                                "GRACIAS POR SU PREFERENCIA" + "<br />" + "<br />";
            return lines;

        }

        /// <summary>
        /// Cliente de configuracion de correo
        /// </summary>
        /// <returns></returns>
        public Boolean clienteCorreo()
        {
            try
            {

                //Creamos un objeto de cliente de correo
                cliente = new SmtpClient();
                //Hay que crear las credenciales del correo emisor
                cliente.Credentials = new NetworkCredential("wellnesslabenformamx@gmail.com", "&Odontologia13");
                /*
                * Cliente SMTP
                * Gmail:  smtp.gmail.com  puerto:587
                * Hotmail: smtp.liva.com  puerto:25
                */
                //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
                cliente.Port = 587;
                cliente.EnableSsl = true;
                //cliente.Port = 465
                cliente.Host = "smtp.gmail.com"; //Para Gmail "smtp.gmail.com";

                //Enviamos el mensaje      
                cliente.Send(mmsg);
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

    }
}
