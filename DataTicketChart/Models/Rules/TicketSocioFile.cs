﻿/*''=============================================
'' Autor:	Faleg A. Peralta
'' Modificado por: 
'' Fecha de Modificación: 06.10.2015
'' Descripcion General: Ticket socio
'' =============================================*/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Web;
using System.Windows;

namespace WebApplication1.Models.Rules
{
    public class TicketSocioFile
    {
        public void imprimirTicket(String NumSocio,String NombreCompleto, String PliegueTricipal, String PliegueEscapular, String Trigliceridos, String Colesterol, 
            String Glucosa, String FrecCardiRepo, String PresArtSisfo, String PresArtSisDias, String PorCargaPecho, String PorCargaPierna,String MetaboBasal)
        {
            try
            {
                
                string[] lines = {   "WELLNESS LAB EN FORMA" + "" ,
                                     "EXPEDIDO EN:" + "<br />" ,
                                     "CALLE ODONTOLOGÍA NO. 13 LOC. 1 COLONIA SPUAZ" + "",
                                     "MEXICO, GPE. ZACATECAS" + "<br />" ,
                                     " " + "<br />" ,
                                     "Socio: " + NumSocio + " "+ NombreCompleto + " " ,
                                     "Fecha de revisión: " + DateTime.Now.ToShortDateString() + "" ,
                                     " " + "*" + "Pliegue Tricipal: " + PliegueTricipal ,
                                     " " + "*" + "Pliegue Escapular: " + PliegueEscapular ,
                                     " " + "*" + "Trigliceridos: " + Trigliceridos ,
                                     " " + "*" + "Colesterol: " + Colesterol ,
                                     " " + "*" + "Glucosa: " + Glucosa ,
                                     " " + "*" + "Frecuencía Cardiaca: " + FrecCardiRepo ,
                                     " " + "*" + "Frecuencía Arterial Sisfolica: " + PresArtSisfo ,
                                     " " + "*" + "Frecuencía Arterial Diasfolica: " + PresArtSisDias ,
                                     " " + "*" + "Frecuencía Arterial Diasfolica: " + PresArtSisDias ,
                                     " " + "*" +
                                     " " + "*" + "% Porcentaje Carga Pecho: " + PorCargaPecho ,
                                     " " + "*" + "% Porcentaje Carga Pierna: " + PorCargaPierna ,
                                     " " + "*" + "Metabolismo Basal: " + MetaboBasal ,
                                     " " + "*" ,
                                     " " + " " ,
                                     " " +  "TU SALUD ES NUESTRA PASION..." + " " ,
                                    "VIVE LA EXPERIENCIA WELLNESS LAB EN FORMA" + " " ,
                                     "GRACIAS POR SU PREFERENCIA" + " "  };

                // Set a variable to the My Documents path.
                string mydocpath = "C:\\Users\\WALLNESS LAB\\Desktop\\Recibos de Pagos";
                //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                // Write the string array to a new file named "WriteLines.txt".
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\Ticket de Progreso_" + NumSocio + "_Socio_ " + NombreCompleto + "_" +DateTime.Now.ToLongDateString()+".txt"))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line);
                    
                }
                Console.WriteLine(mydocpath + @"\Ticket de Progreso_" + NumSocio + "_Socio_ " + NombreCompleto + "_" + DateTime.Now.ToLongDateString() + ".txt");
            }
            catch (Exception ex)
            {

                
            }
            
        }
    }
}