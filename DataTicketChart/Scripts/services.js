﻿function ajaxPostCall(url, data) {
    return $.ajax({
        type: "POST",
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
}


function getClients() {
    ajaxPostCall("../Dataticket/GetClients", '{ }')
        .done(function (response) {
            $('#client option').remove();
            $('#client').append('<option value=0>Seleccione un Socio...</option>');
            $.each(response, function (item, client) {
                $('#client').append('<option value=' + client.id_socio + '>' + client.num_socio + ' --- ' + client.nombre + ' ' + client.ap_paterno + ' ' + client.ap_materno + '</option>');
            });
        })
        .fail(function (ex) {
            console.log(ex.innerHTML);
        });
}